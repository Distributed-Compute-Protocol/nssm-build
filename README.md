# INTRODUCTION

This repository contains scripts to download and release NSSM binaries.

## Getting The Code

To clone the
[NSSM Build repository](https://gitlab.com/Distributed-Compute-Protocol/nssm-build),
enter the following:
```
git clone --recursive git@gitlab.com:Distributed-Compute-Protocol/nssm-build.git
```

## Building

To build, enter the following:
```
mkdir build
cd build
cmake ..
cmake --build .
```

## Deploying

To deploy, clone
[dcp-native-ci](https://gitlab.com/Distributed-Compute-Protocol/dcp-native-ci)
and read the "Deploying" section of the "README.md" file therein.
